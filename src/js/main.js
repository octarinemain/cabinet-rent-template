$(document).ready(function() {
	$('.datetimepicker').datetimepicker({
		format: 'DD MMMM YYYY',
		locale: 'ru',
		defaultDate: moment().format("DD MMMM YYYY"),
	});

	function initSlider (perView, between) {
		new Swiper('.record__head-slider', {
			slidesPerView: perView || 6,
			spaceBetween: between || 20,
			breakpointsInverse: false,
			simulateTouch: false,
			navigation: {
				nextEl: '.record .swiper-button-next',
				prevEl: '.record .swiper-button-prev',
			},
			breakpoints: {
				1199: {
					slidesPerView: perView ? 12 : 5
				},
				991: {
					slidesPerView: perView ? 8 : 3
				},
				767: {
					slidesPerView: perView ? 4 : 2
				},
				480: {
					slidesPerView: perView ? 2 : 1
				}
			}
		});

		new Swiper('.record__main-slider', {
			slidesPerView: perView || 6,
			spaceBetween: between || 20,
			breakpointsInverse: false,
			simulateTouch: false,
			navigation: {
				nextEl: '.record .swiper-button-next',
				prevEl: '.record .swiper-button-prev',
			},
			breakpoints: {
				1199: {
					slidesPerView: perView ? 12 : 5
				},
				991: {
					slidesPerView: perView ? 8 : 3
				},
				767: {
					slidesPerView: perView ? 4 : 2
				},
				480: {
					slidesPerView: perView ? 2 : 1
				}
			}
		});
	}
	initSlider();

	$('.record__main-slider-time').hover(function () {
		handlerActiveTime($(this), 'hover');
	}, function () {
		$('.record__main-slider-time.hover').removeClass('hover');
	});

	$('.record__main-slider-time').on('click', function () {
		handlerActiveTime($(this), 'active');
		$('.popup-record').addClass('active');
		$('body').addClass('no-scrolling');
	});

	function handlerActiveTime ($this, activeClass) {
		$this.addClass(activeClass);

		if ($this.next().is('.disable, .ban') || !$this.next().length) {
			$this.prev().addClass(activeClass);
			return;
		}

		$this.next().addClass(activeClass);
	}

	$('.record__tabs button').on('click', function () {
		if ($(this).hasClass('active')) return;

		$('.record__tabs button').removeClass('active');
		$(this).addClass('active');

		var index = $(this).index();

		$('.record__tabs-content-item')
			.removeClass('active')
			.eq(index)
			.addClass('active');

		if (!index) return initSlider();
		initSlider(14, 8);
	});

	$('.popup__close, .js-popup-close').on('click', function (e) {
		e.preventDefault();

		$('.popup').removeClass('active');
		$('body').removeClass('no-scrolling');
		$('.record__main-slider-time').removeClass('active');
	});

	$('.popup').on('click', function (e) {
		const wrap = $(".popup__inner");
		if (!wrap.is(e.target) && wrap.has(e.target).length === 0) {
			$('.popup').removeClass('active');
			$('body').removeClass('no-scrolling');
			$('.record__main-slider-time').removeClass('active');
		}
	});

	$("#phone").mask("+7(999) 999-9999");
});

module.exports = function () {
	$.gulp.task("scripts-FTP", $.gulp.series("scripts", function () {
		const configFTP = require('./config-FTP')();
		const connect = configFTP.connect;
		const path = configFTP.path;

		return $.gulp.src("./public/js/**", {base: './public/js', buffer: false})
			.pipe(connect.newer(`${path}js`))
			.pipe(connect.dest(`${path}js`))
	}));
};

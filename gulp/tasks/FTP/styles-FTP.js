module.exports = function () {
	$.gulp.task("styles-FTP", $.gulp.series("styles", function () {
		const configFTP = require('./config-FTP')();
		const connect = configFTP.connect;
		const path = configFTP.path;

		return $.gulp.src("./public/styles/**", {base: './public/styles', buffer: false})
			.pipe(connect.newer(`${path}styles`))
			.pipe(connect.dest(`${path}styles`))
	}));
};

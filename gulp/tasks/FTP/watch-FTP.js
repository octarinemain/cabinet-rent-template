module.exports = function () {
    $.gulp.task("watch-FTP", function () {
        return new Promise(res => {
            $.watch("./src/styles/**/*.scss", $.gulp.series("styles-FTP"));
            $.watch("./src/img/**", $.gulp.series("images-FTP"));
            $.watch("./src/js/**/*.js", $.gulp.series("scripts-FTP"));
            $.watch("./src/fonts/*", $.gulp.series("add-FTP"));
            res();
        });
    });
};

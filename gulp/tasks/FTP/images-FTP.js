module.exports = function () {
	$.gulp.task("images-FTP", $.gulp.series("images", function () {
		const configFTP = require('./config-FTP')();
		const connect = configFTP.connect;
		const path = configFTP.path;

		return $.gulp.src(["./public/img/**", "!./public/img/sprites"], {base: './public/img', buffer: false})
			.pipe(connect.newer(`${path}img`))
			.pipe(connect.dest(`${path}img`))
	}));
};

module.exports = function () {
	const add = [
		"src/js/vendor/**/*",
	];

	$.gulp.task("add-js", function () {
		return $.gulp.src(add)
			.pipe($.gulp.dest("./public/js/"))
			.on("end", $.browsersync.reload);
	});
};

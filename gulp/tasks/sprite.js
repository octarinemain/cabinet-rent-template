module.exports = function() {
    $.gulp.task("sprite", function() {
        return $.gulp.src("./src/img/icons/svg/*.svg")
            //.pipe($.replace("&gt;", ">"))
            .pipe($.svgSprite({
				preview: false,
                svgPath: "/img/sprites/svg/sprite.svg",
                padding: 15
            }))
            .pipe($.gulp.dest("./public/img/sprites/"))
            .pipe($.debug({"title": "sprite"}));
    });
};